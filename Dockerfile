FROM python:alpine

RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev
RUN pip3 install flask psycopg2 ConfigParser 

WORKDIR /srv/app

ENTRYPOINT ["python3", "/srv/app/web.py"]
